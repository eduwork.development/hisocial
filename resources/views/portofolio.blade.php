@extends('layouts.main')

@section('container')
    <!-- Hero -->
    <div class="relative">
        <div class="relative w-[100vw-1px] h-[calc(100vh-74px)] lg:h-[calc(100vh-200px)] xl:h-[calc(100vh-280px)] mx-auto">
            <img src="assets/img/portofolio/Background.png" class="absolute w-full h-full object-cover object-center z-10"
                alt="gambar hero">
            <div
                class="absolute flex flex-col justify-center h-[80%] px-6 lg:px-0 text-center inset-0 my-auto md:text-center space-y-4 lg:space-y-6 xl:space-y-7 z-20">
                <h1
                    class="text-3xl lg:text-5xl xl:text-6xl font-bold lg:leading-tight xl:leading-tight text-pb-normal-active">
                    Explore Our Portfolio of <br class="hidden md:block" /><span class="text-p-normal">Digital
                        Excellence</span></h1>
                <p class="text-[24px] border ">Explore our extensive portfolio and discover innovative solutions that can<br>
                    help you achieve your business goals.</p>
            </div>
        </div>

        <div class="hidden lg:block relative px-6 lg:px-0 lg:w-[70%] max-w-[1065px] mx-auto">
            <div
                class="relative -top-16 z-40 lg:flex w-full max-w-[1065px] h-[109px] bg-white drop-shadow-[0px_5px_20px_rgba(0,0,0,0.15)] lg:rounded-2xl">
                <div class="h-full w-full">
                    <button id="button-social" data-menu="socialmediamanagement"
                        class="section-button relative flex-1 w-full h-full p-4 font-normal text-black hover:font-semibold hover:bg-[#0081E6] font-inter transition-all rounded-l-2xl border-r hover:text-white">
                        Social Media<br />Marketing
                    </button>
                    <div
                        class="arrow-socialmediamanagement w-20 h-20 border-t-32 border-r-16 border-b-32 border-l-16 border-blue-900 transform rotate-45 bg-[#0081E6] mx-auto mt-[20px] hidden">
                    </div>
                </div>
                <div class="h-full w-full">
                    <button id="button-branding" data-menu="brandingidentityvisual"
                        class="section-button relative flex-1 w-full h-full p-4 font-normal text-pb-normal-active hover:font-semibold hover:bg-[#0081E6] font-inter transition-all  border-r hover:text-white">
                        Branding Identity<br />Visual
                    </button>
                    <div
                        class="arrow-brandingidentityvisual w-20 h-20 border-t-32 border-r-16 border-b-32 border-l-16 border-blue-900 transform rotate-45 bg-[#0081E6] mx-auto mt-[20px] hidden">
                    </div>
                </div>
                <div class="h-full w-full">
                    <button id="button-logo" data-menu="logobrandingidentity"
                        class="section-button relative flex-1 w-full h-full p-4 font-normal text-pb-normal-active hover:font-semibold hover:bg-[#0081E6] font-inter transition-all border-r hover:text-white">
                        Logo Branding<br />Identity
                    </button>
                    <div
                        class="arrow-logobrandingidentity w-20 h-20 border-t-32 border-r-16 border-b-32 border-l-16 border-blue-900 transform rotate-45 bg-[#0081E6] mx-auto mt-[20px] hidden">
                    </div>
                </div>
                <div class="h-full w-full">
                    <button id="button-website" data-menu="websitesystem"
                        class="section-button relative flex-1 w-full h-full p-4 font-normal text-pb-normal-active hover:font-semibold hover:bg-[#0081E6] font-inter transition-all border-r hover:text-white">
                        Website And<br />System
                    </button>
                    <div
                        class="arrow-websitesystem w-20 h-20 border-t-32 border-r-16 border-b-32 border-l-16 border-blue-900 transform rotate-45 bg-[#0081E6] mx-auto mt-[20px] hidden">
                    </div>
                </div>
                <div class="h-full w-full">
                    <button id="button-design" data-menu="design"
                        class="section-button relative flex-1 w-full h-full p-4 font-normal text-pb-normal-active hover:font-semibold hover:bg-[#0081E6] font-inter transition-all rounded-r-2xl border-r hover:text-white">
                        Design Interior &<br />Exterior
                    </button>
                    <div
                        class="arrow-design w-20 h-20 border-t-32 border-r-16 border-b-32 border-l-16 border-blue-900 transform rotate-45 bg-[#0081E6] mx-auto mt-[20px] hidden">
                    </div>
                </div>
            </div>

            <div style="opacity: 1;"
                class="toolbar absolute inset-x-0 -bottom-20 flex justify-center items-center w-full max-w-[1065px] h-[109px] mx-auto px-10 py-4 bg-[#0081E6] lg:rounded-2xl transition-all duration-500 z-40">
                <p class="texts-socialmediamanagement lg:text-md xl:text-lg text-center text-white font-inter hidden">Where
                    strategy meets engagement, and your brand's digital narrative unfolds with purpose. Driving both
                    engagement and conversion.</p>
                <p class="texts-brandingidentityvisual lg:text-md xl:text-lg text-center text-white font-inter hidden">We
                    define a visual narrative that speaks volumes about your brand's essence. Ready to convert your Leads
                    into Prospects.</p>
                <p class="texts-logobrandingidentity lg:text-md xl:text-lg text-center text-white font-inter hidden">Where
                    strategy meets engagement, and your brand's digital narrative unfolds with purpose. Driving both
                    engagement and conversion.</p>
                <p class="texts-websitesystem lg:text-md xl:text-lg text-center text-white font-inter hidden">From
                    Sleek-modern to Classic-timeless layouts, we prioritize cleanliness to ensure functionality and positive
                    UX in your system.</p>
                <p class="texts-design lg:text-md xl:text-lg text-center text-white font-inter hidden">From contemporary
                    living spaces to inviting outdoor havens, let HiSocial redefine your interior and exterior experience
                </p>
            </div>
        </div>
    </div>
    <!-- Akhir Hero -->


    <section class="w-full lg:mt-[120px] mt-[60px] mb-[80px] px-4 lg:px-0">
        <div class="max-w-[1065px] mx-auto">
            <!-- Social Media Management -->
            <div id="socialmediamanagement" role="tabpanel" aria-labelledby="button-social" class="content">
                <h1 class="lg:hidden block text-center text-[24px] mb-[24px] bg-[#0081E6] text-white p-2 rounded-lg">Social
                    Media Management</h1>
                <div class="grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-center mx-auto justify-center gap-8">
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-mg.png" alt="">
                            <a href="https://www.canva.com/design/DAF3UHod5iM/jRjxP-hHIe4wAIaokdm6dg/view?utm_content=DAF3UHod5iM&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management Maju Group</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-baby.png" alt="">
                            <a href="https://www.canva.com/design/DAF44KO9z8s/0e34Ak1L2x-xQ6Tl4BBXGQ/view?utm_content=DAF44KO9z8s&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management BabyKid</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-cf.png" alt="">
                            <a href="https://www.canva.com/design/DAF3YKJU0aY/WLoLRUkByJwcZlOY3jvQHw/view?utm_content=DAF3YKJU0aY&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management Creative Furniture</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-salenda.png" alt="">
                            <a href="https://www.canva.com/design/DAF4XRk7A_I/5okWAGg-hC6Cuyr_nEt-pw/view?utm_content=DAF4XRk7A_I&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management Salenda</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-avisena.png" alt="">
                            <a href="https://www.canva.com/design/DAF3tIgCz7E/R13rw7n4ldsPo4crFygmaA/view?utm_content=DAF3tIgCz7E&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management Avisena</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/socialmedia/portofolio-sisko.png" alt="">
                            <a href="https://www.canva.com/design/DAF5RnGg-z0/Ur75yu-NGhjo5_lR35nKEg/view?utm_content=DAF5RnGg-z0&utm_campaign=designshare&utm_medium=link&utm_source=editor"
                                target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Instagram & Facebook Management Sisko</p>
                    </div>
                </div>
                <div class="flex lg:flex-row flex-col justify-center lg:gap-4">
                    <a href="https://dribbble.com/hisocialdigital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Dribbble <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                    <a href="https://www.behance.net/hisocial_digital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Behance <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                </div>
            </div>
            <!-- End Social Media Management -->
            <!-- Branding Identity Visual -->
            <div id="brandingidentityvisual" class="content" role="tabpanel" aria-labelledby="button-branding">
                <h1
                    class="lg:hidden block text-center text-[24px] mt-[60px] mb-[24px] bg-[#0081E6] text-white p-2 rounded-lg">
                    Branding Identity Visual</h1>
                <div class="grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-center justify-center gap-8">
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/brandingidentity/portofolio-gf.png" alt="">
                            <a href="https://drive.google.com/file/d/1GWey8OWKnQoEQctuHYoifpQPSnknbr4l/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>Grow Finance</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/brandingidentity/portofolio-doit.png" alt="">
                            <a href="https://drive.google.com/file/d/1GddhvvgpJ3nNf3D8Tkv_Icfu6yGhcLOx/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>MyDoit</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative rounded-[20px]">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/brandingidentity/portofolio-salenda.png" alt="">
                            <a href="https://drive.google.com/file/d/1FfZ0hOtaihldhgcrLJTbmAl6ApX_q6ko/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>Salenda Pisang</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center shadow-[0_3px_10px_rgb(0,0,0,0.2)] rounded-[20px] group relative">
                            <img class="group-hover:brightness-[.3] transition rounded-[20px]"
                                src="assets/img/portofolio/brandingidentity/portofolio-bc.png" alt="">
                            <a href="https://drive.google.com/file/d/1GVU0z_Xe8k-5bCOBZnaU3HHwBSzSoZZC/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>Beauty Clinic</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center shadow-[0_3px_10px_rgb(0,0,0,0.2)] rounded-[20px] group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/brandingidentity/portofolio-hb.png" alt="">
                            <a href="https://drive.google.com/file/d/1Ga39FD3vJZys6p25fb85BFKER1_dUgb_/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>Healthy Bites</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/brandingidentity/portofolio-troisen.png" alt="">
                            <a href="https://drive.google.com/file/d/1FVTPPfORYFVC_11-O2yXuWplawJJQsQV/view?usp=drive_link" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Branding Guidelines for <br>Troissèns</p>
                    </div>
                </div>
                <div class="flex lg:flex-row flex-col justify-center lg:gap-4">
                    <a href="https://dribbble.com/hisocialdigital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Dribbble <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                    <a href="https://www.behance.net/hisocial_digital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Behance <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                </div>
            </div>
            <!-- End Branding Identity Visual -->
            <!-- Logo Branding IDentity -->
            <div id="logobrandingidentity" class="content" role="tabpanel" aria-labelledby="button-logo">
                <h1
                    class="lg:hidden block text-center text-[24px] mt-[60px] mb-[24px] bg-[#0081E6] text-white p-2 rounded-lg">
                    Logo Branding Identity</h1>
                <div class="grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-center justify-center gap-8">
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-elvron.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>Elvron</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-salenda.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>Salenda Pisang</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-palmerah.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>Palmerah Industrial</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-eduwork.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>Eduwork</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-sisko.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>Sisko</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/logobranding/portofolio-doit.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Logo Branding Identity for <br>MyDoit</p>
                    </div>
                </div>
                <div class="flex lg:flex-row flex-col justify-center lg:gap-4">
                    <a href="https://dribbble.com/hisocialdigital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Dribbble <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                    <a href="https://www.behance.net/hisocial_digital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Behance <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                </div>
            </div>
            <!-- End Logo Branding -->
            <!-- Website System -->
            <div id="websitesystem" class="content" role="tabpanel" aria-labelledby="button-website">
                <h1
                    class="lg:hidden block text-center text-[24px] mt-[60px] mb-[24px] bg-[#0081E6] text-white p-2 rounded-lg">
                    Website And System</h1>
                <div class="grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-center justify-center gap-8">
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-gf.png" alt="">
                            <a href="https://dribbble.com/shots/23246937-Grow-Finance-Fintech-Website" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Grow Finance</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-tm.png" alt="">
                            <a href="https://dribbble.com/shots/22528947-Digital-Marketing-Landing-Pages" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Tech Mentor</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-salenda.png" alt="">
                            <a href="https://salenda.id/" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Salenda Pisang</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-upscale.png" alt="">
                            <a href="https://www.upscale.id/" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Upscale</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center shadow-[0_3px_10px_rgb(0,0,0,0.2)] rounded-[20px] group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-hb.png" alt="">
                            <a href="https://dribbble.com/shots/22354492-HealthyBite-Health-Landing-Pages" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Healthy Bites</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/websitesystem/portofolio-troisen.png" alt="">
                            <a href="https://dribbble.com/shots/23264205-Troissens-Website-E-Commerce" target="blank"><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Front-End Development <br>for Troissèns</p>
                    </div>
                </div>
                <div class="flex lg:flex-row flex-col justify-center lg:gap-4">
                    <a href="https://dribbble.com/hisocialdigital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Dribbble <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                    <a href="https://www.behance.net/hisocial_digital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Behance <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                </div>
            </div>
            <!-- End Website System -->

            <!-- Design Interior ex -->
            <div id="design" class="content" role="tabpanel" aria-labelledby="button-design">
                <h1
                    class="lg:hidden block text-center text-[24px] mt-[60px] mb-[24px] bg-[#0081E6] text-white p-2 rounded-lg">
                    Design Interior & Exterior</h1>
                <div class="grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 items-center justify-center gap-8">
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design1.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design2.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design3.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design4.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design5.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                    <div class="flex flex-col justify-center items-center">
                        <div class="text-center  group relative">
                            <img class="group-hover:brightness-[.3] transition"
                                src="assets/img/portofolio/design/design6.png" alt="">
                            <a href=""><button
                                    class="py-3 w-[80%] px-4 rounded-full bg-[#008FFF] text-white font-medium opacity-0 absolute top-[43%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 transition duration-300 ease-in-out group-hover:opacity-100 group-hover:translate-y-0">Lihat
                                    Selengkapnya</button></a>
                        </div>
                        <p class="text-center mt-4 text-[18px]">Interior Design</p>
                    </div>
                </div>
                <div class="flex lg:flex-row flex-col justify-center lg:gap-4">
                    <a href="https://dribbble.com/hisocialdigital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Dribbble <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                    <a href="https://www.behance.net/hisocial_digital" target="blank"><button
                        class="text-white mt-5 lg:mt-10 bg-[#008FFF] hover:bg-blue-600 shadow-3xl rounded-[30px] shadow-blue-400 w-full h-[50px] lg:h-13 lg:w-[263px] flex justify-center items-center font-medium text-[20px]">More
                        On Behance <img class="ml-4" src="assets/icons/arrow-bg.svg" alt=""></button></a>
                </div>
            </div>
            <!-- End Design Interior ex -->
        </div>
    </section>


    <!-- Testimonial -->
    <section class="mx-auto mb-20 mt-8 px-4 lg:px-0 bg-cover py-12"
        style="background-image: url('assets/img/portofolio/Background-blur.png');">
        <div class="flex flex-row gap-1 mx-auto w-fit">
            <img class="w-10" src="assets/icons/arrow-service.svg" alt="">
            <h1 class="text-[32px] font-extrabold">Testimonial</h1>
        </div>
        <h1 class="lg:text-[48px] text-[28px] text-center w-fit mx-auto">What our Happy Client say.</h1>
        <div class="swiper swiperku">
            <div class="swiper-wrapper">
                <div class="swiper-slide w-fit mx-auto mt-6 flex flex-col justify-center items-center gap-6">
                    <img class="w-[300px]" src="assets/icons/star.svg" alt="">
                    <p class="lg:text-[24px] text-[18px] lg:w-[794px] w-full text-center mb-6">HiSocial has elevated our
                        online presence with
                        strategic social media management. Their data-driven approach in crafting content has significantly
                        increased our  customer engagement.</p>
                    <img class="rounded-full mb-6" src="assets/img/profil-testi.png" alt="">
                    <h1 class="lg:text-[24px] text-[18px] text-center text-[#008FFF] font-medium">Alicia Moelemar - CEO of
                        Quantum Innovations
                    </h1>
                </div>
                <div class="swiper-slide w-fit mx-auto mt-6 flex flex-col justify-center items-center gap-6">
                    <img class="w-[300px]" src="assets/icons/star.svg" alt="">
                    <p class="lg:text-[24px] text-[18px] lg:w-[794px] w-full text-center mb-6">HiSocial has elevated our
                        online presence with
                        strategic social media management. Their data-driven approach in crafting content has significantly
                        increased our  customer engagement.</p>
                    <img class="rounded-full mb-6" src="assets/img/profil-testi.png" alt="">
                    <h1 class="lg:text-[24px] text-[18px] text-center text-[#008FFF] font-medium">Alicia Moelemar - CEO of
                        Quantum Innovations
                    </h1>
                </div>
            </div>
            <div class="mt-14">
                <div class="swiper-pagination page"></div>
            </div>
        </div>
    </section>
    <!-- Akhir Testimonial -->

    <!-- Contact Admin -->
    <section class="max-w-[1215px] mx-auto mt-8 mb-12 px-4 lg:px-0">
        <div class="w-full rounded-[20px]  bg-[#002B4C] text-white relative">
            <div
                class="h-full w-[221px] bg-[#187CFF] z-0 right-0 absolute rounded-l-[100px] object-cover rounded-r-[20px] hidden lg:block">
            </div>
            <div class="flex flex-col lg:flex-row z-10 relative lg:p-12 px-12 py-6">
                <h1 class="text-[16px] lg:text-[25px] text-center lg:text-left lg:w-[50%] w-full font-medium">
                    Confused about determining the program according to your
                    company's needs?</h1>
                <div
                    class="lg:w-[50%] w-full flex justify-center mt-6 lg:mt-0 lg:justify-end mr-20 items-center text-black">
                    <button
                        class="bg-white hover:bg-slate-200 px-12 py-2 rounded-[74px] lg:text-[24px] font-medium">Contact
                        Our Team</button>
                </div>

            </div>

        </div>
    </section>
    <!-- End Contact Admin -->

    <script type="module">
        import Swiper from 'https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.mjs'
        const swiperku = new Swiper('.swiperku', {
            slidesPerView: 1,
            pagination: {
                el: '.page',
                clickable: true
            },
        });

        var isPortofolioInitialized = false;

        function Portofolio() {
            var contentElements = document.querySelectorAll(".content");
            var socialManagement = document.getElementById("socialmediamanagement");
            var arrowSocialManagement = document.querySelectorAll(".arrow-socialmediamanagement");
            var textsSocialManagement = document.querySelectorAll(".texts-socialmediamanagement");
            var buttonSocial = document.getElementById("button-social");
            var sectionButtons = document.querySelectorAll(".section-button");

            // Sembunyikan semua konten kecuali socialmediamanagement
            for (var i = 0; i < contentElements.length; i++) {
                contentElements[i].style.display = "none";
            }
            socialManagement.style.display = "block";

            // Tampilkan arrow dan texts pada socialmediamanagement
            for (var j = 0; j < arrowSocialManagement.length; j++) {
                arrowSocialManagement[j].style.display = "block";
            }
            for (var k = 0; k < textsSocialManagement.length; k++) {
                textsSocialManagement[k].style.display = "block";
            }

            // Tambahkan kelas ke button-social
            buttonSocial.classList.add("!text-white");
            buttonSocial.classList.add("bg-[#0081E6]");

            // Tambahkan event listener untuk setiap section-button
            for (var l = 0; l < sectionButtons.length; l++) {
                sectionButtons[l].addEventListener("click", function() {
                    var target = this.getAttribute("data-menu");

                    var arrowElements = document.querySelectorAll("[class^='arrow-']");
                    for (var m = 0; m < arrowElements.length; m++) {
                        arrowElements[m].style.display = "none";
                    }

                    var textsElements = document.querySelectorAll("[class^='texts-']");
                    for (var n = 0; n < textsElements.length; n++) {
                        textsElements[n].style.display = "none";
                    }

                    for (var o = 0; o < contentElements.length; o++) {
                        contentElements[o].style.display = "none";
                    }
                    for (var p = 0; p < sectionButtons.length; p++) {
                        sectionButtons[p].classList.remove("bg-[#0081E6]");
                        sectionButtons[p].classList.remove("!text-white");
                    }

                    document.getElementById(target).style.display = "block";
                    this.classList.add("bg-[#0081E6]");
                    this.classList.add("!text-white");
                    document.querySelector(".arrow-" + target).style.display = "block";
                    document.querySelector(".texts-" + target).style.display = "block";
                });
            }
        }

        document.addEventListener("DOMContentLoaded", function() {
            window.addEventListener("resize", function() {
                if (window.innerWidth > 1024 && !isPortofolioInitialized) {
                    Portofolio();
                    isPortofolioInitialized = true;
                } else if (window.innerWidth <= 1024) {
                    var contentElements = document.querySelectorAll(".content");
                    for (var i = 0; i < contentElements.length; i++) {
                        contentElements[i].style.display = "block";
                    }
                    isPortofolioInitialized = false;
                }
            });

            Portofolio();
        });
    </script>
@endsection
